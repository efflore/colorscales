var UxColor;

(function() {
  "use strict";
  
  /**
   * UxColor object
   *
   * @param <string> str: hex code or css named color
   */
  UxColor = function(str) {
    if (typeof str === 'string'){
      if (str.charAt(0) === '#'){
        this.fromHex(str);
      } else {
        this.fromNamed(str);
      }
    }
    return this;
  };
  
  UxColor.prototype = {
    
    // Attach RGB Converter
    srgbConverter: chromatist.rgb.Converter('sRGB'),
    
    // Attach CIECAM02 Converter
    ccamConverter: chromatist.ciecam.Converter({
      adaptive_luminance: 50,
      discounting: true
    }),
    
    /**
     * Create color from hex code
     *
     * @param <string> str: hex code
     * @return <UxColor> object or null on error
     */
    fromHex: function(str) {
      try {
        this.hex = str,
        this.rgb = chromatist.rgb.from_hex(str);
        var _xyz = this.srgbConverter.to_XYZ(this.rgb);
        var _cam = this.ccamConverter.forward_model(_xyz);
        this.hue = _cam.h,
        this.saturation = _cam.s,
        this.lightness = _cam.J;
        this.chroma = _cam.C;
      } catch (error) {
        return null;
      }
      return this;
    },
    
    /**
     * Create color from CSS named color
     *
     * @param <string> str: color name from http://dev.w3.org/csswg/css-color/#named-colors
     * @return <UxColor> object or null on error
     */
    fromNamed: function(str) {
      var _HEXMAP = {
        aliceblue: '#f0f8ff',
        antiquewhite: '#faebd7',
        aqua: '#00ffff',
        aquamarine: '#7fffd4',
        azure: '#f0ffff',
        beige: '#f5f5dc',
        bisque: '#ffe4c4',
        black: '#000000',
        blanchedalmond: '#ffebcd',
        blue: '#0000ff',
        blueviolet: '#8a2be2',
        brown: '#a52a2a',
        burlywood: '#deb887',
        cadetblue: '#5f9ea0',
        chartreuse: '#7fff00',
        chocolate: '#d2691e',
        coral: '#ff7f50',
        cornflowerblue: '#6495ed',
        cornsilk: '#fff8dc',
        crimson: '#dc143c',
        cyan: '#00ffff',
        darkblue: '#00008b',
        darkcyan: '#008b8b',
        darkgoldenrod: '#b8860b',
        darkgray: '#a9a9a9',
        darkgreen: '#006400',
        darkgrey: '#a9a9a9',
        darkkhaki: '#bdb76b',
        darkmagenta: '#8b008b',
        darkolivegreen: '#556b2f',
        darkorange: '#ff8c00',
        darkorchid: '#9932cc',
        darkred: '#8b0000',
        darksalmon: '#e9967a',
        darkseagreen: '#8fbc8f',
        darkslateblue: '#483d8b',
        darkslategray: '#2f4f4f',
        darkslategrey: '#2f4f4f',
        darkturquoise: '#00ced1',
        darkviolet: '#9400d3',
        deeppink: '#ff1493',
        deepskyblue: '#00bfff',
        dimgray: '#696969',
        dimgrey: '#696969',
        docred: '#c0202d',
        docorange: '#d2730d',
        docyellow: '#f0d402',
        docgreen: '#78bc5a',
        docmint: '#1ba981',
        doccyan: '#82cfd4',
        docblue: '#429bd9',
        docviolet: '#5855a5',
        docpurple: '#9363aa',
        docpink: '#d363a5',
        docgray: '#909090',
        dodgerblue: '#1e90ff',
        firebrick: '#b22222',
        floralwhite: '#fffaf0',
        forestgreen: '#228b22',
        fuchsia: '#ff00ff',
        gainsboro: '#dcdcdc',
        ghostwhite: '#f8f8ff',
        gold: '#ffd700',
        goldenrod: '#daa520',
        gray: '#808080',
        green: '#008000',
        greenyellow: '#adff2f',
        grey: '#808080',
        honeydew: '#f0fff0',
        hotpink: '#ff69b4',
        indianred: '#cd5c5c',
        indigo: '#4b0082',
        ivory: '#fffff0',
        khaki: '#f0e68c',
        lavender: '#e6e6fa',
        lavenderblush: '#fff0f5',
        lawngreen: '#7cfc00',
        lemonchiffon: '#fffacd',
        lightblue: '#add8e6',
        lightcoral: '#f08080',
        lightcyan: '#e0ffff',
        lightgoldenrodyellow: '#fafad2',
        lightgray: '#d3d3d3',
        lightgreen: '#90ee90',
        lightgrey: '#d3d3d3',
        lightpink: '#ffb6c1',
        lightsalmon: '#ffa07a',
        lightseagreen: '#20b2aa',
        lightskyblue: '#87cefa',
        lightslategray: '#778899',
        lightslategrey: '#778899',
        lightsteelblue: '#b0c4de',
        lightyellow: '#ffffe0',
        lime: '#00ff00',
        limegreen: '#32cd32',
        linen: '#faf0e6',
        magenta: '#ff00ff',
        maroon: '#800000',
        mediumaquamarine: '#66cdaa',
        mediumblue: '#0000cd',
        mediumorchid: '#ba55d3',
        mediumpurple: '#9370db',
        mediumseagreen: '#3cb371',
        mediumslateblue: '#7b68ee',
        mediumspringgreen: '#00fa9a',
        mediumturquoise: '#48d1cc',
        mediumvioletred: '#c71585',
        midnightblue: '#191970',
        mintcream: '#f5fffa',
        mistyrose: '#ffe4e1',
        moccasin: '#ffe4b5',
        navajowhite: '#ffdead',
        navy: '#000080',
        oldlace: '#fdf5e6',
        olive: '#808000',
        olivedrab: '#6b8e23',
        orange: '#ffa500',
        orangered: '#ff4500',
        orchid: '#da70d6',
        palegoldenrod: '#eee8aa',
        palegreen: '#98fb98',
        paleturquoise: '#afeeee',
        palevioletred: '#db7093',
        papayawhip: '#ffefd5',
        peachpuff: '#ffdab9',
        peru: '#cd853f',
        pink: '#ffc0cb',
        plum: '#dda0dd',
        powderblue: '#b0e0e6',
        purple: '#800080',
        red: '#ff0000',
        rosybrown: '#bc8f8f',
        royalblue: '#4169e1',
        saddlebrown: '#8b4513',
        salmon: '#fa8072',
        sandybrown: '#f4a460',
        seagreen: '#2e8b57',
        seashell: '#fff5ee',
        sienna: '#a0522d',
        silver: '#c0c0c0',
        skyblue: '#87ceeb',
        slateblue: '#6a5acd',
        slategray: '#708090',
        slategrey: '#708090',
        snow: '#fffafa',
        springgreen: '#00ff7f',
        steelblue: '#4682b4',
        tan: '#d2b48c',
        teal: '#008080',
        thistle: '#d8bfd8',
        tomato: '#ff6347',
        turquoise: '#40e0d0',
        uired: '#b81f3b',
        uiorange: '#cd590f',
        uigold: '#e5a210',
        uiolive: '#979f0e',
        uigreen: '#1f7b23',
        uimint: '#18c193',
        uicyan: '#19cbcc',
        uiazure: '#19b8ec',
        uiblue: '#2160ec',
        uiviolet: '#7c3aec',
        uipurple: '#c438e9',
        uipink: '#c92093',
        uigray: '#a3a3a3',
        violet: '#ee82ee',
        wheat: '#f5deb3',
        white: '#ffffff',
        whitesmoke: '#f5f5f5',
        yellow: '#ffff00',
        yellowgreen: '#9acd32'
      };
      return this.fromHex(_HEXMAP[str]);
    },
    
    /**
     * Create color from CIECAM02 tupel
     *
     * @param <object> arr: [lightness, chroma, hue]
     * @return <UxColor> object or null on error
     */
    fromCAM: function(arr) {
      try {
        var _xyz = this.ccamConverter.reverse_model({J: arr[0], C: arr[1], h: arr[2]}).XYZ;
        this.rgb = this.srgbConverter.from_XYZ(_xyz);
        this.hex = chromatist.rgb.to_hex(this.rgb);
        this.lightness  = arr[0];
        this.chroma     = arr[1];
        this.hue        = arr[2];
      } catch (error) {
        return null;
      }
      return this;
    },
    
    /*
    fromRGB: function(arr) {
      
    }, */
    
    /**
     * Calculate color contrast ratio according to WCAG 2.0 success criterion 1.4.3
     *
     * @param <UxColor> col: background color
     * @return <float> contrast ratio:
     *   - 3 passes AA for large text (above 18pt or bold above 14pt)
     *   - 4.5 passes AA level for any size text and AAA for large text (above 18pt or bold above 14pt)
     *   - 7 passes AAA level for any size text
     */
    calcContrast: function(col) {
      var _lum1 = this.luminance,
        _lum2 = col.luminance;
      if (_lum1 > _lum2) {
        return ((_lum1 + 0.05) / (_lum2 + 0.05));
      } else {
        return ((_lum2 + 0.05) / (_lum1 + 0.05));
      }
    },
    
    // Get CIECAM02 tupel: [lightness, chroma, hue]
    get cam() {
      return [this.lightness, this.chroma, this.hue];
    },
    
    // Set CIECAM02 tupel: [lightness, chroma, hue]
    set cam(arr) {
      this.fromCAM(arr);
    },
    
    // Get relative luminance
    get luminance() {
      var _FACTOR = [0.2126, 0.7152, 0.0722];
      
      var _transform = function(value) {
        if (value <= 0.03928) {
          return value / 12.92;
        } else {
          return Math.pow((value + 0.055) / 1.055, 2.4);
        }
      };
      
      return _FACTOR[0] * _transform(this.rgb[0]) + _FACTOR[1] * _transform(this.rgb[1]) + _FACTOR[2] * _transform(this.rgb[2]);
    },
    
    toString: function() {
      return this.hex;
    },
    
    clone: function() {
      return new UxColor(this.hex);
    }
    
  };

})();
