/**
 * UxColorWheel object
 *
 * @param <UxColor> base: primary color
 * @param <integer> num: number of steps
 */
var UxColorWheel = function(base, num) {
  var _this = this;
  this.length = (typeof num === 'number' ? num : 12);
  this.base   = base;
  this.wheel  = [];
  
  var _calcHue = function(x) {
    return (((_this.base.hue / 360) + x) % 1) * 360;
  };
  
  /**
   * Get maximum chroma in sRGB gamut for given lightness and hue
   */
  var _maxChroma = function(l, h) {
    var _c = 0, _newC, _test;
    for (var _i = 0; _i < 10; _i++) {
      _newC = _c + 60 * Math.pow(2, -_i);
      _test = new UxColor().fromCAM([l, _newC, h]);
      if ((_test === null) || !_test.hasOwnProperty('hex')) { // out of gamut
        continue;
      }
      _c = _newC;
    }
    return _c;
  };
  
  var _s = this.base.chroma / _maxChroma(this.base.lightness, this.base.hue), _h;
  for (var i = 0; i < num; i++) {
    _h = _calcHue(i / num);
    this.wheel[i] = new UxColor().fromCAM([this.base.lightness, _s * _maxChroma(this.base.lightness, _h), _h]);
  }
  
  return this;
};