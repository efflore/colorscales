/**
 * UxColorScale object
 *
 * @param <UxColor> base: middle color of color scale
 * @param <integer> num: number of steps
 */
var UxColorScale = function(base, num) {
  var _this = this;
  this.length = (typeof num === 'number' ? num : 10);
  this.base   = base;
  this.gamma = Math.log(100 / this.base.lightness) / Math.log(2);
  this.exp   = [];
  this.sin   = [[], [], [], [], []];
  this.gray  = [];
  this.gamut = [];
  this.outOfGamut = false;
  
  var _calcLightness = function(x) {
    return 100 * Math.pow(x, _this.gamma);
  };
  
  var _calcExpChroma = function(x) {
    if (x > 0.5){
      x = 1 - x;
    }
    return _this.base.chroma * (Math.exp(2 * x) - 1) / (Math.E - 1);
  };
  
  var _calcSinChroma = function(x, exp) {
    var _exp2 = (exp + 1) / 2;
    return _this.base.chroma * (Math.pow(2, _exp2) * Math.pow(Math.sin(Math.PI * (4 * x + 1) / 6), _exp2) - 1) * ((Math.exp(exp / 5) - 1) / ((Math.pow(2, _exp2) - 1) * (Math.E - 1)));
  };
  
  /**
   * Get maximum chroma in sRGB gamut for given lightness and hue
   */
  var _maxChroma = function(l, h) {
    var _c = 0, _newC, _test;
    for (var _i = 0; _i < 10; _i++) {
      _newC = _c + 60 * Math.pow(2, -_i);
      _test = new UxColor().fromCAM([l, _newC, h]);
      if ((_test === null) || !_test.hasOwnProperty('hex')) { // out of gamut
        continue;
      }
      _c = _newC;
    }
    return _c;
  };
  
  /**
   * Create color with given lightness fitting into the color scale
   *
   * @param <float> lightness: lightless from 0 (black) to 100 (white)
   */
  this.getTone = function(lightness) {
    var x = 1 - Math.tan(Math.PI * Math.pow(1 - (lightness / 100), (1 / this.gamma)) / 4); // inverse function of calcLightness
    var c = _calcSinChroma(x, 5);
    return new UxColor().fromCAM([lightness, c, this.base.hue]);
  };
  
  var l, expC, sinC, i, j;
  for (i = 1; i < num; i++) {
    l = _calcLightness(i / num);
    expC = _calcExpChroma(i / num);
    this.exp[i - 1] = new UxColor().fromCAM([l, expC, this.base.hue]);
    for (j = 0; j < 5; j++) {
      sinC = _calcSinChroma(i / num, j + 1);
      this.sin[j][i - 1] = new UxColor().fromCAM([l, sinC, this.base.hue]);
    }
    this.gray[i - 1] = new UxColor().fromCAM([l, 0, 0]);
    if ((this.sin[4][i - 1] === null) || (this.exp[i - 1] === null)) {
      this.outOfGamut = true;
    }
  }
  var n = 100;
  for (i = 1; i < n; i++) {
    this.gamut[i - 1] = _maxChroma(100 * i / n, this.base.hue);
  }
  
  return this;
};