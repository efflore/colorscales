/* globals: currentColorView */
var currentColorView;

$(document).ready(function() {
  
  var _calcColors = function(hex) {
    var color = new UxColor(hex);
    var scale = new UxColorScale(color, 20);
    var wheel = new UxColorWheel(color, 24);
    currentColorView = new UxColorView('#colorContainer', scale, wheel);
  };
  
  var _drawColors = function() {
    if (typeof currentColorView !== 'object') {
      _calcColors($('#uxColorHexInput').length ? $('#uxColorHexInput').val() : '#ee0022');
    } else {
      currentColorView.draw();
    }
  };
  
  // on arrow keys
  $(window).on('keydown', function(e) {
    var color = new UxColor(),
      scale,
      diff = (e.shiftKey ? 10 : 1);
    switch (e.keyCode){
      case 38: // up -> increase lightness
        color.fromLCH([currentColorScale.base.lightness + diff, currentColorScale.base.chroma, currentColorScale.base.hue]);
        break;
      case 40: // down -> decrease lightness
        color.fromLCH([currentColorScale.base.lightness - diff, currentColorScale.base.chroma, currentColorScale.base.hue]);
        break;
      case 37: // left -> decrease chroma
        color.fromLCH([currentColorScale.base.lightness, currentColorScale.base.chroma - diff, currentColorScale.base.hue]);
        break;
      case 39: // right -> increase chroma
        color.fromLCH([currentColorScale.base.lightness, currentColorScale.base.chroma + diff, currentColorScale.base.hue]);
        break;
      default: // do nothing
        return;
    }
    if (color.hex !== '#000000'){ // ignore if base color out of gamut
      currentColorView.setBaseColor(color.hex);
    }
  });
  
  // on resize
  $(window).on('resize', function(e) {
    _drawColors();
  });
  
  // on load
  _drawColors();
  
});