/* global currentColorView */
var currentColorView;

/**
 * UxColorView object
 *
 * @param <string> container: CSS selector of container to append HTML output to
 * @param <UxColorScale> scale: color tones of the same hue
 * @param <UxColorWheel> wheel: complementary color of the same lightness and saturation
 */
var UxColorView = function(container, scale, wheel) {
  this.container = container;
  this.scale = scale;
  this.wheel = wheel;
  
  var lang = {
    hexLabel:        'Hex-Code',
    rgbLabel:        'RGB-Werte',
    hueLabel:        'Buntton',
    chromaLabel:     'Buntheit',
    saturationLabel: 'Sättigung',
    lightnessLabel:  'Helligkeit',
    applyLink:       'Übernehmen',
    appTitle:        'Colorscales',
    contrastTitle:   'Kontrasttöne'
  };
  
  var _lightnessClass = function(color) {
    if (1.05 / (color.luminance + 0.05) >= 4.5) { // enough contrast to white, according to WCAG 2.0 success criterion 1.4.3
      return 'dark';
    } else {
      return 'light';
    }
  };
  
  var _rgbValue = function(v) {
    return Math.round(255 * v);
  };
  
  this.drawGamut = function(dimensions) {
    return '<li style="' + dimensions + '">&nbsp;</li>';
  };
  
  this.drawColor = function(color, position, classes) {
    return '<li class="color ' + _lightnessClass(color) + (classes ? ' ' + classes : '') + '" ' +
      'style="background: ' + color.hex + ';' + position + '">&nbsp;</li>';
  };
  
  this.drawOffColor = function(classes) {
    return '<li class="color off' + (classes ? ' ' + classes : '') + '">&nbsp;</li>';
  };
  
  this.setBaseColorInfo = function(color) {
    document.title = color.hex + ' · ' + lang.appTitle;
    $('#uxBaseColorSwatch').css('background', color.hex).attr('class', _lightnessClass(color));
    $('#uxColorHexInput').val(color.hex);
    $('#uxColorRGB .rgb-r').text(_rgbValue(color.rgb[0]));
    $('#uxColorRGB .rgb-g').text(_rgbValue(color.rgb[1]));
    $('#uxColorRGB .rgb-b').text(_rgbValue(color.rgb[2]));
    $('#uxColorHueInput').val(color.hue.toFixed(2));
    $('#uxColorChromaInput').val(color.chroma.toFixed(2));
    $('#uxColorLightnessInput').val(color.lightness.toFixed(2));
  };
  
  this.setScaleInfo = function(scale, scaleType) {
    var _white = new UxColor('#ffffff'),
        _black = new UxColor('#000000');
    
    var _scaleStep = function(color, classes) {
      
      var _contrastTone = function(altColor) {
        return '<li class="contrast-color" style="color: ' + altColor.hex + ';">' +
          '<span class="swatch" style="background: ' + altColor.hex + '">&nbsp;</span><span class="value">' + altColor.hex + '</span>' +
          '</li>';
      };
      
      if (color !== null) {
        var _html, _j;
        _html = '<li class="color' + classes + '">' +
          '<div class="swatch ' + _lightnessClass(color) + '" style="background: ' + color.hex + ';">&nbsp;</div>' +
          '<div class="info"><dl>' +
          '<dt>' + lang.hexLabel + '</dt><dd class="hex"><span><span class="value">' + color.hex + '</span> <a href="#" title="' + lang.applyLink + '" class="apply">➜</a></span></dd>' +
          '<dt>' + lang.rgbLabel + '</dt><dd><span>' + _rgbValue(color.rgb[0]) + ', ' + _rgbValue(color.rgb[1]) + ', ' + _rgbValue(color.rgb[2]) + '</span></dd>' +
          '<dt class="margin">' + lang.hueLabel + '</dt><dd class="margin"><span>' + color.hue.toFixed(2) +'°</span></dd>' +
          '<dt>' + lang.chromaLabel + '</dt><dd><span>' + color.chroma.toFixed(2) +'</span></dd>' +
          '<dt>' + lang.lightnessLabel + '</dt><dd><span>' + color.lightness.toFixed(2) +'%</span></dd>' +
          '</dl></div>';
        if (scaleType !== 'wheel') {
          _html += '<div class="contrast"><p><strong>' + lang.contrastTitle + '</strong></p><ul style="background: ' + color.hex + ';">';
          if (color.calcContrast(_white) >= 4.5) {
            _html += _contrastTone(_white);
          }
          for (_j = scale.length - 1; _j >= 0; _j--) {
            if ((scale[_j] !== null) && (color.calcContrast(scale[_j]) >= 4.5)) {
              _html += _contrastTone(scale[_j]);
            }
          }
          if (color.calcContrast(_black) >= 4.5) {
            _html += _contrastTone(_black);
          }
          _html += '</ul></div>';
        }
        _html += '</li>';
        return _html;
      } else {
        return '<li class="color off' + classes + '">&nbsp;</li>';
      }
    };
    
    var _output = '<ul data-type="' + scaleType + '">', _i;
    if (scaleType === 'wheel') {
      for (_i = 0; _i < scale.length; _i++) {
        _output += _scaleStep(scale[_i], _i % 2 !== 0 ? ' small' : '');
      }
    } else {
      for (_i = scale.length - 1; _i >= 0; _i--) {
        _output += _scaleStep(scale[_i], _i % 2 === 0 ? ' small' : '');
      }
    }
    _output += '</ul>';
    $('#colorScales .slide.' + scaleType).html(_output);
  };
  
  this.draw = function() {
    $(this.container).html('<div id="uxColorScale"></div><div id="uxColorWheel"></div>');
    $(this.container).css({
      height: $(window).height(),
      width: $(window).width() - 320
    });
    $('#colorInfo').css({
      height: $(window).height()
    });
    $('#uxColorScale').css({
      height: $(this.container).height() - 50
    });
    this.drawScale();
    this.drawWheel();
    this.init();
  };
  
  this.draw();
};

/**
 * Set new base color
 */
UxColorView.prototype.setBaseColor = function(color, callback) {
  if ((color === null) || !color.hasOwnProperty('hex')) { // ignore if base color out of gamut
    if (callback && (typeof callback.error === 'object')) {
      callback.error();
    }
    return;
  }
  this.scale = new UxColorScale(color, 20);
  this.wheel = new UxColorWheel(color, 24);
  this.draw();
  location.hash = color.hex.substr(1);
  if (callback && (typeof callback.success === 'object')) {
    callback.success();
  }
};

/**
 * Bind events
 */
UxColorView.prototype.init = function() {
  
  // click on color swatch
  $('li.color').on('click', function(e) {
    e.stopPropagation();
    $('li.color').removeClass('active');
    $('#colorScales .slide').addClass('hidden');
    var _scaleType = $(this).closest('ul').attr('data-type');
    var _i = $(this).index();
    $('#colorScales ul[data-type="' + _scaleType + '"]').closest('.slide').removeClass('hidden');
    $('ul[data-type="' + _scaleType + '"] :nth-child(' + (_i + 1) + ')').addClass('active');
  });
  
  // click on apply link
  $('li.color a.apply').on('click', function(e) {
    e.preventDefault();
    e.stopPropagation();
    currentColorView.setBaseColor(new UxColor($(this).parent().find('.value').text()));
  });
  
};

/**
 * Draw color tones
 */
UxColorView.prototype.drawScale = function() {
  var i, html, c, _this = this,
    mid = Math.round((this.scale.exp.length - 1) / 2),
    width = $('#uxColorScale').width() - 50,
    height = $('#uxColorScale').height() - 50;
    
  var _getPosition = function(color) {
    return ' left: ' + Math.round(width * color.chroma / 115 + 4) + 'px; top: ' + Math.round(height * (100 - color.lightness) / 100 + 4) + 'px;';
  };
  
  var _drawTones = function(scale, classes){
    var _output = '', _i, _c;
    for (_i = scale.length - 1; _i >= 0; _i--) {
      _c = scale[_i];
      if (_c !== null) {
        _output += _this.drawColor(_c, _getPosition(_c), (_i % 2 === 0 ? 'small' : ''));
      } else {
        _output += _this.drawOffColor((_i % 2 === 0 ? 'small' : ''));
      }
    }
    return _output;
  };
  
  this.setBaseColorInfo(this.scale.base);
  var n = this.scale.gamut.length;
  html = '<ul class="gamut">';
  for (i = n; i > 0; i--) {
    html += this.drawGamut('top: ' + Math.round(height * (100 - 100 * i / (n + 1)) / 100 + 24) + 'px; width: ' + Math.round(width * this.scale.gamut[i - 1] / 115 + 4) + 'px;');
  }
  html += '</ul><ul class="bw">';
  html += '<li class="color light white" style="background: #fff; top: 4px; left: 4px;">';
  html += '<li class="color dark black" style="background: #000; top: ' + (height + 4) + 'px; left: 4px;">';
  html += '</ul><ul class="gray" data-type="gray">';
  html += _drawTones(this.scale.gray);
  this.setScaleInfo(this.scale.gray, 'gray');
  for (i = 0; i < 5; i++) {
    html += '</ul><ul class="sin sin' + (i + 1) + '" data-type="sin' + (i + 1) + '">';
    html += _drawTones(this.scale.sin[i]);
    this.setScaleInfo(this.scale.sin[i], 'sin' + (i + 1));
  }
  html += '</ul><ul class="exp" data-type="exp">';
  var classes;
  for (i = this.scale.exp.length - 1; i >= 0; i--) {
    c = this.scale.exp[i];
    if (c !== null) {
      if (i === mid) {
        classes = 'base';
      } else if (i % 2 === 0) {
        classes = 'small';
      } else {
        classes = '';
      }
      html += this.drawColor(c, _getPosition(c), classes);
    } else {
      html += this.drawOffColor((i % 2 === 0 ? 'small' : ''));
    }
  }
  html += '</ul>';
  this.setScaleInfo(this.scale.exp, 'exp');
  $('#uxColorScale').html(html);
};

/**
 * Draw complementary colors
 */
UxColorView.prototype.drawWheel = function() {
  var i, html, c,
    width = $('#uxColorWheel').width() - 50;
    
  var _getPosition = function(color) {
    return ' left: ' + Math.round(width * color.hue / 360 + 4) + 'px;';
  };
  
  html = '<ul class="wheel" data-type="wheel">';
  for (i = 0; i < this.wheel.wheel.length; i++) {
    c = this.wheel.wheel[i];
    if (c !== null) {
      html += this.drawColor(c, _getPosition(c), (i % 2 !== 0 ? 'small' : ''));
    }
  }
  html += '</ul>';
  this.setScaleInfo(this.wheel.wheel, 'wheel');
  $('#uxColorWheel').html(html);
};

/**
 * Bind window events
 */
$(document).ready(function() {
  var _hashchangeEnabled = true;
  
  var _colorViewFactory = function(hex) {
    var color = new UxColor(hex);
    var scale = new UxColorScale(color, 20);
    var wheel = new UxColorWheel(color, 24);
    return new UxColorView('#colorContainer', scale, wheel);
  };
  
  var _fromHash = function() {
    var hex = location.hash.split('#')[1]; // get from URL
    if (hex) {
      var num = parseInt(hex, 16);
      if (!isNaN(num) && (num >= 0) && (num <= 16777216)) {
        return '#' + hex;
      }
    }
    return false;
  };
  
  var _drawColors = function(base) {
    if (typeof currentColorView !== 'object') {
      var hex = base ? base : _fromHash();
      var selected = hex ? hex : '#bb2222';
      currentColorView = _colorViewFactory(selected);
    } else if (base) {
      var color = new UxColor(base);
      currentColorView.setBaseColor(color);
    } else {
      currentColorView.draw();
    }
  };
  
  // on hash change
  $(window).on('hashchange', function(e) {
    if (_hashchangeEnabled) {
      var hex = _fromHash();
      if (hex) {
        _hashchangeEnabled = false;
        _drawColors(hex);
      }
    } else {
      _hashchangeEnabled = true;
    }
  });
  
  // on arrow keys
  $(window).on('keydown', function(e) {
    if ($(e.target).is('input')) {
      return; // ignore input fields
    }
    var color = new UxColor(),
      diff = (e.shiftKey ? 10 : 1);
    switch (e.keyCode) {
      case 38: // up -> increase lightness
        color.fromCAM([currentColorView.scale.base.lightness + diff, currentColorView.scale.base.chroma, currentColorView.scale.base.hue]);
        break;
      case 40: // down -> decrease lightness
        color.fromCAM([currentColorView.scale.base.lightness - diff, currentColorView.scale.base.chroma, currentColorView.scale.base.hue]);
        break;
      case 37: // left -> decrease chroma
        color.fromCAM([currentColorView.scale.base.lightness, currentColorView.scale.base.chroma - diff, currentColorView.scale.base.hue]);
        break;
      case 39: // right -> increase chroma
        color.fromCAM([currentColorView.scale.base.lightness, currentColorView.scale.base.chroma + diff, currentColorView.scale.base.hue]);
        break;
      case 107: // plus or period -> increase chroma
      case 190:
        color.fromCAM([currentColorView.scale.base.lightness, currentColorView.scale.base.chroma, currentColorView.scale.base.hue + diff]);
        break;
      case 109: // minus or dash -> decrease chroma
      case 189:
        color.fromCAM([currentColorView.scale.base.lightness, currentColorView.scale.base.chroma, currentColorView.scale.base.hue - diff]);
        break;
      default: // do nothing
        return;
    }
    _hashchangeEnabled = false;
    currentColorView.setBaseColor(color, {
      success: function () {
        _hashchangeEnabled = true;
      },
      error: function () {
        _hashchangeEnabled = true;
      }
    });
  });
  
  // on hex input change
  $('#uxColorHexInput').on('change', function(e) {
    currentColorView.setBaseColor(new UxColor($('#uxColorHexInput').val()));
  });
  
  // on HSL input change
  $('.hslInput').on('change', function(e) {
    try {
      var color = new UxColor().fromCAM([parseFloat($('#uxColorLightnessInput').val()), parseFloat($('#uxColorChromaInput').val()), parseFloat($('#uxColorHueInput').val())]);
      currentColorView.setBaseColor(color, {
        error: function () {
          console.log('outofgamut');
        }
      });
    } catch (error) {
      console.log('outofgamut');
    }
  });
  
  // on decrease hue button
  $('#uxColorInputForm .button-pair button').on('click', function(e) {
    var color = new UxColor(),
      diff = (e.shiftKey ? 10 : 1);
    switch ($(this).attr('id')) {
      case 'uxColorLightnessIncrease': // up -> increase lightness
        color.fromCAM([currentColorView.scale.base.lightness + diff, currentColorView.scale.base.chroma, currentColorView.scale.base.hue]);
        break;
      case 'uxColorLightnessDecrease': // down -> decrease lightness
        color.fromCAM([currentColorView.scale.base.lightness - diff, currentColorView.scale.base.chroma, currentColorView.scale.base.hue]);
        break;
      case 'uxColorChromaDecrease': // left -> decrease chroma
        color.fromCAM([currentColorView.scale.base.lightness, currentColorView.scale.base.chroma - diff, currentColorView.scale.base.hue]);
        break;
      case 'uxColorChromaIncrease': // right -> increase chroma
        color.fromCAM([currentColorView.scale.base.lightness, currentColorView.scale.base.chroma + diff, currentColorView.scale.base.hue]);
        break;
      case 'uxColorHueDecrease': // left -> decrease chroma
        color.fromCAM([currentColorView.scale.base.lightness, currentColorView.scale.base.chroma, currentColorView.scale.base.hue - diff]);
        break;
      case 'uxColorHueIncrease': // right -> increase chroma
        color.fromCAM([currentColorView.scale.base.lightness, currentColorView.scale.base.chroma, currentColorView.scale.base.hue + diff]);
        break;
      default: // do nothing
        return;
    }
    _hashchangeEnabled = false;
    currentColorView.setBaseColor(color, {
      success: function () {
        _hashchangeEnabled = true;
      },
      error: function () {
        _hashchangeEnabled = true;
      }
    });
  });
  
  // on input form submit
  $('#uxColorInputForm').on('submit', function(e) {
    e.preventDefault();
  });
  
  // on resize
  $(window).on('resize', function(e) {
    _drawColors();
  });
  
  // on load
  _drawColors();
  
});